<?php
namespace Fifty\PublisherAPI;

class API {
    public static $api_key = null;

    public static function setApiKey($api_key) {
        static::$api_key = $api_key;
    }
}
