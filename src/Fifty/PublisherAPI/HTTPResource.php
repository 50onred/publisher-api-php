<?php
namespace Fifty\PublisherAPI;

class HTTPResource {
    public static function fetchReport($endpoint, $params) {
        if (is_null($api_key = API::$api_key)) {
            throw new FiftyAPIException('No API key set.');
        }
        $options = array(
            'http' => array(
                'ignore_errors' => true
            )
        );
        $ctx = stream_context_create($options, $params);
        $fp = new \SplFileObject("https://$api_key:@pubapi.50onred.com/v2/$endpoint?" . http_build_query($params), 'r', false, $ctx);

        if (!static::check200($http_response_header)) {
            $contents = '';
            while (!$fp->eof()) {
                $contents .= $fp->fgets();
            }
            $contents = json_decode($contents, true);
            throw new FiftyAPIException(json_decode($contents['error'], true));
        }

        return $fp;
    }

    private static function check200($headers) {
        foreach ($headers as $header) {
            if (strpos($header, '200 OK') !== false) {
                return true;
            }
        }
        return false;
    }
}
