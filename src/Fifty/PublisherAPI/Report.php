<?php
namespace Fifty\PublisherAPI;

class Report {
    public static function custom($params) {
        return HTTPResource::fetchReport('report', $params);
    }

    public static function zones($start_date, $end_date, $pubtype) {
        return HTTPResource::fetchReport('zones', array(
            'start_date' => $start_date,
            'end_date' => $end_date,
            'pubtype' => $pubtype
        ));
    }
}
