# 50onRed Publisher API PHP bindings

You can get an API key from your [publisher dashboard](https://publisher.50onred.com/account/developer-api).

## Requirements

PHP 5.3.3 and later.

## Composer

You can install the bindings via [Composer](http://getcomposer.org/). Add this to your `composer.json`:

    {
      "require": {
        "50onred/publisher-api": "2.*"
      }
    }

Then install via:

    composer install
    
Or, do it in one shot with:

    composer require "50onred/publisher-api:2.*"

To use the bindings, use Composer's [autoload](https://getcomposer.org/doc/00-intro.md#autoloading):

    require_once('vendor/autoload.php');

## Manual Installation

If you do not wish to use Composer, you can download the [latest release](https://bitbucket.org/50onred/publisher-api-php/downloads). Then, to use the bindings, include the `API.php` file in the `src` directory.

    require_once('/path/to/50onred/publisher-api-php/src/Fifty/PublisherAPI/API.php');

## Getting Started

Simple usage looks like:
```php
\Fifty\PublisherAPI\API::setApiKey('your_api_key');
$parameters = array(
    'filters' => array(
        'zone' => array('example_zone_name'), //Optional
        'geo' => array('US', 'CA') //Optional
        'monetization' => array('Banners', 'CA') //Optional
    ),
    'group_by' => array('monetization', 'date'), //Optional
    'start_date' => '2015-01-01', //Required
    'end_date' => '2015-01-02', //Required
    'pubtype' => 'js' //Required
);

$report = \Fifty\PublisherAPI\Report::custom($parameters); // returns a `SplFileObject`

while (!$report->eof()) {
    var_dump($report->fgetcsv());
}


```

## Documentation

Please see https://publisher.50onred.com/api-docs/v2/index.html for up-to-date documentation.